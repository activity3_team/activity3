import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

class McDWebsiteTest {

	WebDriver driver;
    final String CHROMEDRIVER_LOCATION = "/Users/harman/Desktop/chromedriver";
	
	
	final String URL_TO_TEST = "https://www.mcdonalds.ca";
	
	@BeforeEach
	void setUp() throws Exception {
		
		System.setProperty("webdriver.chrome.driver","/Users/harman/Desktop/chromedriver");
		driver = new ChromeDriver();
		
		driver.get(URL_TO_TEST);
        System.out.println("This is setup");
	}

	@AfterEach
	void tearDown() throws Exception {
		Thread.sleep(5000);
		driver.close();
	}

	@Test
	void testCaseOne() {
		fail("Not yet implemented");
	}
	
	@Test
	void testCaseTwo() {
		fail("Not yet implemented");
	}
	
	@Test
	void testCaseThree() {
		//fail("Not yet implemented");
	}

}
